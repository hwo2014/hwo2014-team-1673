/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.util.Scanner;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;


/**
 *
 * @author nikkhouy
 */
public class MainTest {
    
    public MainTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of main method, of class Main.
     */
    @org.junit.Test
    public void testMain() throws Exception {
//        Scanner scn = new Scanner(new File("src/main/java/data.json"));
//        String str = "";
//        while(scn.hasNext())
//            str += scn.next();
        
        JsonNode jsn = new ObjectMapper().readTree(new File("src/test/java/resources/data.json"));
        JsonNode jsn2 = jsn.get("data").get("race").get("track").get("pieces");
        System.out.println(jsn2);
    }
    
}
