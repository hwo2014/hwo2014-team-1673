package noobbot;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * @author Behrouz Derakhshan
 */
public class LogicTest {


    @Test
    public void testTrack() throws IOException {

        JsonNode jsn = new ObjectMapper().readTree(new File("src/test/java/resources/data.json"));

        Logic logic = new Logic();

        logic.process(jsn.toString());

        System.out.println(logic.getTrack());
        
        
    }
    
    @Test
    public void carTest() throws IOException {
        JsonNode jsn = new ObjectMapper().readTree(new File("src/test/java/resources/yourcar.json"));
        JsonNode jsn2 = new ObjectMapper().readTree(new File("src/test/java/resources/carPositions.json"));

        Logic logic = new Logic();

        logic.process(jsn.toString());
        logic.process(jsn2.toString());
        
        System.out.println(logic.getCar());
    }
}
