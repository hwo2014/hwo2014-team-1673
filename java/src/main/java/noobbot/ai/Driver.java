package noobbot.ai;

import noobbot.map.Car;
import noobbot.map.Track;

/**
 * @author Behrouz Derakhshan
 */
public interface Driver {
    public double findThrottle(Car car, Track track);
}
