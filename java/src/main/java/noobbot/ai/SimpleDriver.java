package noobbot.ai;

import noobbot.map.Car;
import noobbot.map.PieceType;
import noobbot.map.Track;

/**
 * @author Behrouz Derakhshan
 *
 * Find the best Throttle based on the current and next piece
 */
public class SimpleDriver implements Driver {
    @Override
    public double findThrottle(Car car, Track track) {
        int currentIndex = car.getPieceIndex();
        double inPieceDistance = car.getInPieceDistance();

        if(track.get(currentIndex).getType().equals(PieceType.Bend))
            return 0.62;
        else if(track.distanceToNextBend(car) > 100.0)
            return 1.0;
        else if(track.getNext(currentIndex).getType().equals(PieceType.Straight))
            return 0.8;

        return 0.62;
    }
}
