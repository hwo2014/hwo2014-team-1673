package noobbot.map;

/**
 * @author Behrouz Derakhshan
 */
public enum PieceType {
    Bend,
    Straight;
}
