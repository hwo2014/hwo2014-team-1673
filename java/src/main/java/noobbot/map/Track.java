package noobbot.map;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Behrouz Derakhshan
 */
public class Track {
    String id;
    String name;
    List<Piece> pieceList;

    public Track(String id, String name) {
        pieceList = new LinkedList<>();
        this.id = id;
        this.name = name;
    }


    public void add(Piece piece) {
        pieceList.add(piece);
    }

    public Piece get(int index){
        return pieceList.get(index % pieceList.size());
    }

    public Piece getNext(int index){
        return pieceList.get((index + 1) % pieceList.size());
    }

    public Piece get(Car car){
        return pieceList.get(car.getPieceIndex());
    }

    public double distanceToNextBend(Car car){
        int cur = car.getPieceIndex();
        if(get(cur).getType().equals(PieceType.Bend))
            return 0;

        Straight p = (Straight)get(cur);
        double distance = p.getLength() - car.getInPieceDistance();
        cur++;
        while(!get(cur).getType().equals(PieceType.Bend)){
           distance+= ((Straight)get(cur)).getLength();
        }

        return distance;
    }

    @Override
    public String toString(){
        return "id : " + id + " , name = " + name + " , pieces : " + pieceList.toString();
    }
}
