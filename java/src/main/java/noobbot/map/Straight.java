package noobbot.map;

/**
 * @author Behrouz Derakhshan
 */
public class Straight extends Piece {
    public boolean isSwitchTrack() {
        return switchTrack;
    }

    public double getLength() {
        return length;
    }

    private final boolean switchTrack;
    private final double length;

    public Straight(double length) {
        this(length, false);
    }

    public Straight(double length, boolean switchTrack) {
        this.length = length;
        this.switchTrack = false;
        setType(PieceType.Straight);
    }

    @Override
    public String toString(){
        return "length : " + length + " , switch : " + switchTrack;
    }
}
