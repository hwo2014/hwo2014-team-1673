/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package noobbot.map;

/**
 *
 * @author nikkhouy
 */
public class Car {

    private String name;
    private String color;
    private double angle;
    private int pieceIndex;
    private double inPieceDistance;
    private int startLaneIndex;
    private int endLaneIndex;

    public Car(String name, String color) {
        this.color = color;
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    @Override
    public String toString(){
        return getName()+" " + getColor() + " " + getAngle() + " " + getPieceIndex() + " " + getInPieceDistance();
    }
    
    public String getColor() {
        return color;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public int getPieceIndex() {
        return pieceIndex;
    }

    public void setPieceIndex(int pieceIndex) {
        this.pieceIndex = pieceIndex;
    }

    public double getInPieceDistance() {
        return inPieceDistance;
    }

    public void setInPieceDistance(double inPieceDistance) {
        this.inPieceDistance = inPieceDistance;
    }

    public int getStartLaneIndex() {
        return startLaneIndex;
    }

    public void setStartLaneIndex(int startLaneIndex) {
        this.startLaneIndex = startLaneIndex;
    }

    public int getEndLaneIndex() {
        return endLaneIndex;
    }

    public void setEndLaneIndex(int endLaneIndex) {
        this.endLaneIndex = endLaneIndex;
    }

}
