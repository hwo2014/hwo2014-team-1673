package noobbot.map;

/**
 * @author Behrouz Derakhshan
 */
public class Bend extends Piece {
    private final double angle;

    public double getRadius() {
        return radius;
    }

    public double getAngle() {
        return angle;
    }

    private final double radius;

    public Bend(double radius, double angle){
        this.radius = radius;
        this.angle = angle;
        setType(PieceType.Bend);
    }

    @Override
    public String toString(){
        return "radius : " + radius + " , angle : " + angle;
    }
}
