package noobbot.map;

/**
 * @author Behrouz Derakhshan
 */
public abstract class Piece {
    PieceType type;

    public void setType(PieceType type) {
        this.type = type;
    }

    public PieceType getType() {
        return type;
    }
}
