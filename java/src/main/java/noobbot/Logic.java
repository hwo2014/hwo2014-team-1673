package noobbot;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import noobbot.map.Bend;
import noobbot.map.Piece;
import noobbot.map.Straight;
import noobbot.map.Track;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import noobbot.map.Car;
import noobbot.map.Crash;

/**
 * @author Behrouz Derakhshan
 */
public class Logic {

    private static ObjectMapper MAPPER = new ObjectMapper();
    private MessageParser messageParser = new MessageParser();
    private Track track;
    private Car car;
    private List<Crash> crashHistory;

    enum MessageType {

        none, gameInit, carPositions, yourCar, crash
    };

    public void process(String message) throws IOException {
        JsonNode root = MAPPER.readTree(message);
        MessageType messageType;
        try {
            messageType = MessageType.valueOf(root.get("msgType").asText());
        } catch (Exception exce) {
            messageType = MessageType.none;
        }

        switch (messageType) {
            case gameInit:
                this.track = processTrack(root.get("data").get("race").get("track"));
                break;
            case carPositions:
                processCarPositions(findMyCar(root.get("data")));
                break;
            case yourCar:
                processCar(root.get("data"));
                break;
            case crash:
                processCrash(root.get("data"));
                break;

        }

    }

    public Car getCar() {
        return car;
    }

    public Track getTrack() {
        return track;
    }

    // current position
    public void getState(String state) throws IOException {

    }

    private void processCrash(JsonNode crashJson) {
        if (this.crashHistory == null) {
            this.crashHistory = new LinkedList<>();
        }
        if (crashed(this.car, crashJson)) {
            this.crashHistory.add(new Crash(copyCar()));
        }
    }

    private Car copyCar() {
        Car copy = new Car(this.car.getName(), this.car.getColor());
        copy.setAngle(this.car.getAngle());
        copy.setEndLaneIndex(this.car.getEndLaneIndex());
        copy.setInPieceDistance(this.car.getInPieceDistance());
        copy.setPieceIndex(this.car.getPieceIndex());
        copy.setStartLaneIndex(this.car.getStartLaneIndex());
        return copy;

    }

    private boolean crashed(Car car, JsonNode data) {
        if (car.getName().equalsIgnoreCase(data.get("name").asText())
                && car.getColor().equalsIgnoreCase(data.get("color").asText())) {
            return true;
        }
        return false;

    }

    private void processCarPositions(JsonNode carJson) {
        System.out.println(carJson);
        car.setAngle(carJson.get("angle").asDouble());
        car.setEndLaneIndex(carJson.get("piecePosition").get("lane").get("endLaneIndex").asInt());
        car.setInPieceDistance(carJson.get("piecePosition").get("inPieceDistance").asDouble());
        car.setPieceIndex(carJson.get("piecePosition").get("pieceIndex").asInt());
        car.setStartLaneIndex(carJson.get("piecePosition").get("lane").get("startLaneIndex").asInt());

    }

    private JsonNode findMyCar(JsonNode positionJson) {

        for (JsonNode node : positionJson) {
            JsonNode carJson = node.get("id");
            if (carJson.get("name").asText().equals(car.getName()) && carJson.get("color").asText().equals(car.getColor())) {
                return node;
            }
        }
        throw new IllegalArgumentException("Car not Found");
    }

    private void processCar(JsonNode carJson) {

        car = new Car(carJson.get("name").asText(), carJson.get("color").asText());
    }

    private Track processTrack(JsonNode jsonTrack) {
        Track track = new Track(jsonTrack.get("id").asText(),
                jsonTrack.get("name").asText());

        JsonNode pieces = jsonTrack.get("pieces");

        for (JsonNode node : pieces) {
            Piece piece;
            if (node.has("length")) {
                double length = node.get("length").asDouble();
                boolean switchTrack = false;
                if (node.has("switch")) {
                    switchTrack = node.get("switch").asBoolean();
                }
                piece = new Straight(length, switchTrack);
            } else {
                piece = new Bend(node.get("radius").asDouble(), node.get("angle").asDouble());
            }
            track.add(piece);
        }

        return track;
    }

}
